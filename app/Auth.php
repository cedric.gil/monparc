<?php

/**
 * Class Auth
 */
class Auth
{

    /**
     * @var Auth
     */
    private static $_instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Database $db
     * @param Config $config
     * @return Auth
     */
    public static function getInstance($db, $config){
        if(is_null(self::$_instance)){
            self::$_instance = new Auth($db, $config);
        }
        return self::$_instance;
    }

    /**
     * Auth constructor.
     * @param Database $db
     * @param Config $config
     */
    private function __construct($db, $config){
        $this->db = $db;
        $this->config = $config;
    }

    /**
     * @param string $login
     * @param string $pass
     * @param string $email
     * Vérifie si l'utilisateur peut être créé, puis le crée
     * @return bool|string
     */
    public function register($login, $email, $pass){
        if($login != $pass){
            if(strlen($pass) >= 8 && preg_match('/[A-Z]/', $pass) && preg_match('/[0-9]/', $pass)){
                $pass = hash('SHA512', $pass.$this->config->get('gds'));
                $login = strtolower($login);
                $email = strtolower($email);
                $this->db->checkInjection([$login, $email, $pass]);
                if($this->exist(['login' => $login, 'email' => $email]) === false){
                    $this->db->insert("INSERT INTO users (login, pwd, email) VALUES (?, ?, ?)", [$login, $pass, $email]);
                    return true;
                }
                else{
                    return 'exist';
                }
            }
            else{
                return 'low';
            }
        }
        else{
            return 'same';
        }
    }

    /**
     * @param array $array
     * Vérifie si une information existe déjà
     * @return bool
     */
    private function exist($array){
        $query = "SELECT * FROM users WHERE";
        foreach($array as $key => $value){
            if(isset($or)){
                $query = $query." OR";
            }
            $query = $query." $key = '$value'";
            $or = true;
        }
        if(sizeof($this->db->select($query)) > 0){
            return true;
        }
        return false;
    }

    /**
     * @param string $login
     * @param string $pass
     * Vérifie si les informations d'authentification correspondent à un utilisateur
     * @return bool
     */
    public function login($login, $pass){
        $pass = hash('SHA512', $pass.$this->config->get('gds'));
        $login = strtolower($login);
        $this->db->checkInjection([$login, $pass]);
        $query = "SELECT * FROM users WHERE login = '$login' AND pwd = '$pass'";
        if(sizeof($this->db->select($query)) > 0){
            return true;
        }
        else{
            return 'bad_login';
        }
    }

}

?>