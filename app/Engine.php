<?php

/**
 * Class Engine
 */
class Engine
{

    /**
     * @var Engine
     */
    private static $_instance;

    /**
     * Crée une nouvelle instance si celle-ci n'est pas déjà créé
     * @return Engine
     */
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new Engine();
        }
        return self::$_instance;
    }

    /**
     * @param string $page
     * @param string $spage
     * Retourne le rendu Twig
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function TwigRender($page, $spage, $paramsfromphpfile){
        return Twig::getInstance()->getRender($page, $spage, $paramsfromphpfile);
    }

    /**
     * @return Config
     */
    private function getConfig(){
        return Config::getInstance();
    }

    /**
     * @return Database
     */
    public function getDb(){
        return Database::getInstance($this->getConfig());
    }

    /**
     * @param string $login
     * @param string $email
     * @param string $pass
     * Inscrit un utilisateur si tout est validé
     * @return bool|mixed|string
     */
    public function register($login, $email, $pass){
        $try = Auth::getInstance($this->getDb(), $this->getConfig())->register($login, $email, $pass);
        if($try === true){
            Session::getInstance()->setValue('login', $login);
        }
        return $try;
    }

    /**
     * @param string $login
     * @param string $pass
     * Connecte un utilisateur si tout est validé
     * @return bool|mixed
     */
    public function login($login, $pass){
        $try = Auth::getInstance($this->getDb(), $this->getConfig())->login($login, $pass);
        if($try === true){
            Session::getInstance()->setValue('login', $login);
        }
        return $try;
    }

    /**
     * Vérifie si un utilisateur est connecté
     * @return false|mixed
     */
    public function logged(){
        return Session::getInstance()->getValue('login');
    }

    public function getPhpPath($page){
        return Twig::getInstance()->getPhpPath($page);
    }

}

?>