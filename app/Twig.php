<?php

/**
 * Class Twig
 */
class Twig
{

    /**
     * @var Twig
     */
    private static $_instance;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @return Twig
     */
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = New Twig();
        }
        return self::$_instance;
    }

    /**
     * Twig constructor.
     */
    private function __construct(){
        $loader = new Twig\Loader\FilesystemLoader('../pages');
        $this->twig = new Twig\Environment($loader, [
            'cache' => false
        ]);
    }

    /**
     * @param string $page
     * @param string $spage
     * @param array $paramsfromphpfile
     * Retourne la page demandée
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getRender($page, $spage, $paramsfromphpfile){
        $params = array_merge($this->getParams($page), $paramsfromphpfile);
        switch ($page){
            case 'home':
                return $this->twig->render('home.twig', $params);
            case 'login':
                return $this->twig->render('login.twig', $params);
            case 'register':
                return $this->twig->render('register.twig', $params);
            case 'logout':
                return '';
            case 'windows':
                switch ($spage){
                    case 'dhcp':
                        return $this->twig->render('windows/dhcp.twig', $params);
                    case 'ad_ds':
                        return $this->twig->render('windows/ad_ds.twig', $params);
                    case 'dns':
                        return $this->twig->render('windows/dns.twig', $params);
                    case 'wamp':
                        return $this->twig->render('windows/wamp.twig', $params);
                    case 'wds':
                        return $this->twig->render('windows/wds.twig', $params);
                    case 'filezilla':
                        return $this->twig->render('windows/filezilla.twig', $params);
                    case 'glpi':
                        return $this->twig->render('windows/glpi.twig', $params);
                    default:
                        return $this->twig->render('windows/index.twig', $params);
                }
            case 'linux':
                switch ($spage){
                    default:
                        return $this->twig->render('linux/index.twig', $params);
                }
            case 'pfsense':
                switch ($spage){
                    case 'install':
                        return $this->twig->render('pfsense/install.twig', $params);
                    default:
                        return $this->twig->render('pfsense/index.twig', $params);
                }
            case 'cisco':
                switch ($spage){
                    default:
                        return $this->twig->render('cisco/index.twig', $params);
                }
            case 'vmware':
                switch ($spage){
                    case 'install':
                        return $this->twig->render('vmware/install.twig', $params);
                    case 'create_vm':
                        return $this->twig->render('vmware/create_vm.twig', $params);
                    default:
                        return $this->twig->render('vmware/index.twig', $params);
                }
            default:
                return $this->twig->render('404.twig', $params);
        }
    }

    /**
     * @param string $page
     * @param string $spage
     * Retourne les paramètres relatifs à la page
     * @return array|bool[]|false[]
     */
    private function getParams($page, $spage = ''){
        if(Engine::getInstance()->logged() != false){
            $logged = true;
        }
        else{
            $logged = false;
        }
        switch ($page){
            case 'login':case 'register':
                if($logged === true){
                    header("Location: ?p=home");
                }
                return [];
            case 'windows':case 'linux':case 'pfsense':case 'cisco':case 'vmware':
                if($logged === false){
                    header("Location: ?p=register&tuto=1");
                }
                return ['logged' => $logged];
            default:
                return ['logged' => $logged];
        }
    }

    /**
     * @param string $page
     * Retourne le chemin du fichier PHP lié à la page
     * @return false|string
     */
    public function getPhpPath($page){
        switch ($page){
            case 'login':
                return '../pages/php/login.php';
            case 'logout':
                return '../pages/php/logout.php';
            case 'register':
                return '../pages/php/register.php';
            default:
                return false;
        }
    }

}