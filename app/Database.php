<?php

/**
 * Class Database
 */
class Database
{
    
    /**
     * @var Database
     */
    private static $_instance;

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     * Crée l'instance si celle-ci n'existe pas
     * @return Database
     */
    public static function getInstance($config){
        if(is_null(self::$_instance)){
            self::$_instance = new Database($config);
        }
        return self::$_instance;
    }

    /**
     * @param Config $config
     * Crée l'instance PDO
     */
    private function __construct($config){
        $this->config = $config;
        try{
            $this->pdo = new PDO('mysql:dbname='.$this->config->get('db_name').';host='.$this->config->get('db_host'), $this->config->get('db_user'), $this->config->get('db_pass'));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo 'Echec de la connexion à la base de données : '.$e->getMessage();
        }
    }

    /**
     * @param array $arrayvar
     * Vérifie la présence d'une potentielle tentative d'injection SQL
     */
    public function checkInjection($arrayvar = []){
        foreach ($arrayvar as $var){
            if(stristr($var ,'\'') || stristr($var ,'"') || stristr($var ,'=') || stristr($var ,';') || stristr($var ,'/')){
                exit(0);
            }
        }
    }

    /**
     * @param string $query
     * @param array $params
     * Exécute une commande INSERT
     */
    public function insert($query, $params){
        $stmt = $this->pdo->prepare($query);
        foreach ($params as $key => &$param) {
            $stmt->bindParam($key+1, $param);
        }
        $stmt->execute();
    }

    /**
     * @param string $query
     * Retourne le résultat de la requête
     * @return array
     */
    public function select($query){
        $stmt = $this->pdo->query($query);
        return $stmt->fetchAll();
    }
}

?>