<?php

if(isset($_POST['login']) && isset($_POST['password'])) {
    $login_try = Engine::getInstance()->login($_POST['login'], $_POST['password']);
    if($login_try === true) {
        header("Location: ?p=home");
    }

    $params = [
        'login_try' => $login_try
    ];
}

?>