<?php

if(isset($_POST['login']) && isset($_POST['email']) && isset($_POST['password'])){
    $register_try = Engine::getInstance()->register($_POST['login'], $_POST['email'], $_POST['password']);
    if($register_try === true) {
        header("Location: ?p=home");
    }

    $params = [
        'register_try' => $register_try
    ];
}

if(isset($_GET['tuto'])){
    $params = [
        'tuto' => 1
    ];
}

?>