var menuBody = document.getElementById('menu')
var menuBtn = document.getElementById('icon_menu')

menuBody.style.display = 'flex'
var menuOpen = false
var menuBasePos = '-' + menuBody.offsetWidth + 'px'
menuBody.style.left = menuBasePos

menuBtn.addEventListener('click', activeMenu)
function activeMenu() {
    if (menuOpen === false) {
        transition.begin(menuBody, "transform translateX(0) translateX(" + menuBody.offsetWidth + 'px' + ") 0.4s")
        transition.begin(menuBtn, "transform rotate(0) rotate(-90deg) 0.4s")
        menuBtn.style.position = 'fixed'
        menuOpen = true
    }
    else {
        transition.begin(menuBody, "transform translateX(" + menuBody.offsetWidth + 'px' + ") translateX(0) 0.4s")
        transition.begin(menuBtn, "transform rotate(-90deg) rotate(0) 0.4s")
        menuBtn.style.position = 'relative'
        menuOpen = false
    }
}
window.onresize = resize
function resize() {
    menuBasePos = '-' + menuBody.offsetWidth + 'px';
    menuBody.style.left = menuBasePos
}