<?php

// PHP HEAD

session_start();

ini_set('display_errors', 'on');

if(isset($_GET['p'])){
    $page = $_GET['p'];
    if(isset($_GET['sp'])){
        $spage = $_GET['sp'];
    }
    else{
        $spage = null;
    }
}
else{
    $page = 'home';
    $spage = null;
}

require '../vendor/autoload.php';
require '../app/Autoloader.php';
MyAutoloader::register();

// END PHP HEAD

$phppath = Engine::getInstance()->getPhpPath($page);
$paramsfromphpfile = [];
if($phppath != false){
    require $phppath;
    if(isset($params)){
        $paramsfromphpfile = $params;
    }
}

echo Engine::getInstance()->TwigRender($page, $spage, $paramsfromphpfile);

?>